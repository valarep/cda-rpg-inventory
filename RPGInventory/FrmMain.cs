﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPGInventory
{
    public partial class FrmMain : Form
    {
        public Dictionary<string, Image> Pictures { get; set; }

        private FrmInventory frmInventory;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            FrmLoad frmLoad = new FrmLoad();
            DialogResult result = frmLoad.ShowDialog();
            if (result == DialogResult.OK)
            {
                Pictures = frmLoad.Pictures;
            }
        }

        private void FrmMain_Shown(object sender, EventArgs e)
        {
            Connexion();
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void déconnexionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Connexion();
        }

        private void Connexion()
        {
            FrmLogin frmLogin = new FrmLogin();
            DialogResult result = frmLogin.ShowDialog();
            if (result == DialogResult.Cancel)
            {
                Application.Exit();
            }
        }

        private void inventaireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (frmInventory == null || frmInventory.IsDisposed)
            {
                frmInventory = new FrmInventory();
                frmInventory.Pictures = Pictures;
            }
            frmInventory.Show();
        }
    }
}
