﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPGInventory.classes;

namespace RPGInventory
{
    public enum Bag { Bag1 = 1, Bag2, Bag3, Bag4}
    public partial class FrmInventory : Form
    {
        public Dictionary<string, Image> Pictures { get; set; }

        private Bag ActiveInventory;
        private Button ActiveButton;

        private PictureBox[,] PictureBoxes = new PictureBox[6, 6];
        private Item[,] ItemsBag1 = new Item[6, 6];
        private Item[,] ItemsBag2 = new Item[6, 6];
        private Item[,] ItemsBag3 = new Item[6, 6];
        private Item[,] ItemsBag4 = new Item[6, 6];
        public FrmInventory()
        {
            InitializeComponent();
        }

        private void FrmInventory_Load(object sender, EventArgs e)
        {
            btnBag1.Tag = Bag.Bag1;
            btnBag2.Tag = Bag.Bag2;
            btnBag3.Tag = Bag.Bag3;
            btnBag4.Tag = Bag.Bag4;

            ActiveInventory = Bag.Bag1;
            ActiveButton = btnBag1;
            btnBag1.BackgroundImage = Properties.Resources.btnBag1_active;

            for(int x = 0; x < 6; x++)
            {
                for(int y = 0; y < 6; y++)
                {
                    int num = (x + 1) + 6 * y;
                    PictureBox pictureBox = (PictureBox)pnlInventory.Controls["pbInventory" + num];
                    PictureBoxes[x, y] = pictureBox;
                }
            }

            Item[] items = new Item[]
            {
                new Item() { Name = "item_01", Image = Pictures["objet-amulet-physical-protection-34x34.png"] },
                new Item() { Name = "item_02", Image = Pictures["objet-belt-1-34x34.png"] },
                new Item() { Name = "item_03", Image = Pictures["objet-boots-1-34x34.png"] },
                new Item() { Name = "item_04", Image = Pictures["objet-cuirass-1-34x34.png"] },
                new Item() { Name = "item_05", Image = Pictures["objet-glove-1-34x34.png"] },
                new Item() { Name = "item_06", Image = Pictures["objet-helmet-1-34x34.png"] },
                new Item() { Name = "item_07", Image = Pictures["objet-leg-1-34x34.png"] },
                new Item() { Name = "item_08", Image = Pictures["objet-relic-idol-of-arcane-terror-34x34.png"] },
                new Item() { Name = "item_09", Image = Pictures["objet-ring-1-34x34.png"] },
                new Item() { Name = "item_10", Image = Pictures["objet-shield-3-34x34.png"] },
                new Item() { Name = "item_11", Image = Pictures["objet-shoulder-1-34x34.png"] },
                new Item() { Name = "item_12", Image = Pictures["objet-sword-blue-rotate-34x34.png"] },
            };

            ItemsBag1[0, 0] = items[0];
            ItemsBag1[1, 1] = items[1];
            ItemsBag1[2, 2] = items[2];
            ItemsBag1[3, 3] = items[3];
            ItemsBag1[4, 4] = items[4];
            ItemsBag1[5, 5] = items[5];

            ItemsBag2[0, 5] = items[6];
            ItemsBag2[1, 4] = items[7];
            ItemsBag2[2, 3] = items[8];
            ItemsBag2[3, 2] = items[9];
            ItemsBag2[4, 1] = items[10];
            ItemsBag2[5, 0] = items[11];

            LoadInventory(ActiveInventory);
        }

        private void btnBag_MouseHover(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if ((Bag)button.Tag != ActiveInventory)
            {
                switch (button.Tag)
                {
                    case Bag.Bag1:
                        button.BackgroundImage = Properties.Resources.btnBag1_focus;
                        break;
                    case Bag.Bag2:
                        button.BackgroundImage = Properties.Resources.btnBag2_focus;
                        break;
                    case Bag.Bag3:
                        button.BackgroundImage = Properties.Resources.btnBag3_focus;
                        break;
                    case Bag.Bag4:
                        button.BackgroundImage = Properties.Resources.btnBag4_focus;
                        break;
                }
            }
        }

        private void btnBag_MouseLeave(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if ((Bag)button.Tag != ActiveInventory)
            {
                switch (button.Tag)
                {
                    case Bag.Bag1:
                        button.BackgroundImage = Properties.Resources.btnBag1_inactive;
                        break;
                    case Bag.Bag2:
                        button.BackgroundImage = Properties.Resources.btnBag2_inactive;
                        break;
                    case Bag.Bag3:
                        button.BackgroundImage = Properties.Resources.btnBag3_inactive;
                        break;
                    case Bag.Bag4:
                        button.BackgroundImage = Properties.Resources.btnBag4_inactive;
                        break;
                }
            }
        }

        private void btnBag_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            switch (ActiveButton.Tag)
            {
                case Bag.Bag1:
                    ActiveButton.BackgroundImage = Properties.Resources.btnBag1_inactive;
                    break;
                case Bag.Bag2:
                    ActiveButton.BackgroundImage = Properties.Resources.btnBag2_inactive;
                    break;
                case Bag.Bag3:
                    ActiveButton.BackgroundImage = Properties.Resources.btnBag3_inactive;
                    break;
                case Bag.Bag4:
                    ActiveButton.BackgroundImage = Properties.Resources.btnBag4_inactive;
                    break;
            }


            ActiveInventory = (Bag)button.Tag;
            ActiveButton = button;

            switch (button.Tag)
            {
                case Bag.Bag1:
                    button.BackgroundImage = Properties.Resources.btnBag1_active;
                    break;
                case Bag.Bag2:
                    button.BackgroundImage = Properties.Resources.btnBag2_active;
                    break;
                case Bag.Bag3:
                    button.BackgroundImage = Properties.Resources.btnBag3_active;
                    break;
                case Bag.Bag4:
                    button.BackgroundImage = Properties.Resources.btnBag4_active;
                    break;
            }

            RefreshInventory();
            LoadInventory(ActiveInventory);
        }

        private void RefreshInventory()
        {
            foreach (PictureBox pictureBox in pnlInventory.Controls)
            {
                pictureBox.Image = null;
            }
        }

        private void LoadInventory(Bag bag)
        {
            Item[,] itemsBag = null;
            switch (bag)
            {
                case Bag.Bag1:
                    itemsBag = ItemsBag1;
                    break;
                case Bag.Bag2:
                    itemsBag = ItemsBag2;
                    break;
                case Bag.Bag3:
                    itemsBag = ItemsBag3;
                    break;
                case Bag.Bag4:
                    itemsBag = ItemsBag4;
                    break;
            }

            for (int x = 0; x < 6; x++)
            {
                for(int y = 0; y < 6; y++)
                {
                    if (itemsBag[x, y] != null)
                    {
                        PictureBoxes[x, y].Image = itemsBag[x, y].Image;
                    }
                }
            }
        }
    }
}
