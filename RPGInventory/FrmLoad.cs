﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace RPGInventory
{
    public partial class FrmLoad : Form
    {
        public Dictionary<string, Image> Pictures = new Dictionary<string, Image>();
        public FrmLoad()
        {
            InitializeComponent();
        }

        private void FrmLoad_Shown(object sender, EventArgs e)
        {
            Refresh();
            string targetDirectory = Application.StartupPath + @"\images\objets";
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            progressBar1.Maximum = fileEntries.Length;
            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName);
            }
            DialogResult = DialogResult.OK;
        }

        private void ProcessFile(string path)
        {
            Console.WriteLine("Processed file '{0}'.", path);
            string filename = Path.GetFileName(path);
            lblFileName.Text = filename;
            Image image = Image.FromFile(path);
            Pictures.Add(filename, image);
            progressBar1.Value++;
            Refresh();
        }
    }
}