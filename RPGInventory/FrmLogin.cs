﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPGInventory
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            string goodLogin = "admin";
            string goodPassword = "admin";

            if (txtLogin.Text == goodLogin 
                && txtPassword.Text == goodPassword)
            {
                goodPassword = "12345";
                txtPassword.Text = null;
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Login or password error", 
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtLogin.Text = "";
                txtPassword.Text = "";
                txtLogin.Focus();
            }
        }
    }
}
