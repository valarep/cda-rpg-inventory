﻿namespace RPGInventory
{
    partial class FrmInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbInventory1 = new System.Windows.Forms.PictureBox();
            this.pnlInventory = new System.Windows.Forms.Panel();
            this.pbInventory36 = new System.Windows.Forms.PictureBox();
            this.pbInventory35 = new System.Windows.Forms.PictureBox();
            this.pbInventory34 = new System.Windows.Forms.PictureBox();
            this.pbInventory33 = new System.Windows.Forms.PictureBox();
            this.pbInventory32 = new System.Windows.Forms.PictureBox();
            this.pbInventory31 = new System.Windows.Forms.PictureBox();
            this.pbInventory30 = new System.Windows.Forms.PictureBox();
            this.pbInventory29 = new System.Windows.Forms.PictureBox();
            this.pbInventory28 = new System.Windows.Forms.PictureBox();
            this.pbInventory27 = new System.Windows.Forms.PictureBox();
            this.pbInventory26 = new System.Windows.Forms.PictureBox();
            this.pbInventory25 = new System.Windows.Forms.PictureBox();
            this.pbInventory24 = new System.Windows.Forms.PictureBox();
            this.pbInventory23 = new System.Windows.Forms.PictureBox();
            this.pbInventory22 = new System.Windows.Forms.PictureBox();
            this.pbInventory21 = new System.Windows.Forms.PictureBox();
            this.pbInventory20 = new System.Windows.Forms.PictureBox();
            this.pbInventory19 = new System.Windows.Forms.PictureBox();
            this.pbInventory18 = new System.Windows.Forms.PictureBox();
            this.pbInventory17 = new System.Windows.Forms.PictureBox();
            this.pbInventory16 = new System.Windows.Forms.PictureBox();
            this.pbInventory15 = new System.Windows.Forms.PictureBox();
            this.pbInventory14 = new System.Windows.Forms.PictureBox();
            this.pbInventory13 = new System.Windows.Forms.PictureBox();
            this.pbInventory12 = new System.Windows.Forms.PictureBox();
            this.pbInventory11 = new System.Windows.Forms.PictureBox();
            this.pbInventory10 = new System.Windows.Forms.PictureBox();
            this.pbInventory9 = new System.Windows.Forms.PictureBox();
            this.pbInventory8 = new System.Windows.Forms.PictureBox();
            this.pbInventory7 = new System.Windows.Forms.PictureBox();
            this.pbInventory6 = new System.Windows.Forms.PictureBox();
            this.pbInventory5 = new System.Windows.Forms.PictureBox();
            this.pbInventory4 = new System.Windows.Forms.PictureBox();
            this.pbInventory3 = new System.Windows.Forms.PictureBox();
            this.pbInventory2 = new System.Windows.Forms.PictureBox();
            this.btnBag1 = new System.Windows.Forms.Button();
            this.btnBag2 = new System.Windows.Forms.Button();
            this.btnBag3 = new System.Windows.Forms.Button();
            this.btnBag4 = new System.Windows.Forms.Button();
            this.pnlButtons = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory1)).BeginInit();
            this.pnlInventory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory2)).BeginInit();
            this.pnlButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbInventory1
            // 
            this.pbInventory1.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory1.Location = new System.Drawing.Point(20, 11);
            this.pbInventory1.Name = "pbInventory1";
            this.pbInventory1.Size = new System.Drawing.Size(34, 34);
            this.pbInventory1.TabIndex = 0;
            this.pbInventory1.TabStop = false;
            // 
            // pnlInventory
            // 
            this.pnlInventory.BackColor = System.Drawing.Color.Transparent;
            this.pnlInventory.Controls.Add(this.pbInventory36);
            this.pnlInventory.Controls.Add(this.pbInventory35);
            this.pnlInventory.Controls.Add(this.pbInventory34);
            this.pnlInventory.Controls.Add(this.pbInventory33);
            this.pnlInventory.Controls.Add(this.pbInventory32);
            this.pnlInventory.Controls.Add(this.pbInventory31);
            this.pnlInventory.Controls.Add(this.pbInventory30);
            this.pnlInventory.Controls.Add(this.pbInventory29);
            this.pnlInventory.Controls.Add(this.pbInventory28);
            this.pnlInventory.Controls.Add(this.pbInventory27);
            this.pnlInventory.Controls.Add(this.pbInventory26);
            this.pnlInventory.Controls.Add(this.pbInventory25);
            this.pnlInventory.Controls.Add(this.pbInventory24);
            this.pnlInventory.Controls.Add(this.pbInventory23);
            this.pnlInventory.Controls.Add(this.pbInventory22);
            this.pnlInventory.Controls.Add(this.pbInventory21);
            this.pnlInventory.Controls.Add(this.pbInventory20);
            this.pnlInventory.Controls.Add(this.pbInventory19);
            this.pnlInventory.Controls.Add(this.pbInventory18);
            this.pnlInventory.Controls.Add(this.pbInventory17);
            this.pnlInventory.Controls.Add(this.pbInventory16);
            this.pnlInventory.Controls.Add(this.pbInventory15);
            this.pnlInventory.Controls.Add(this.pbInventory14);
            this.pnlInventory.Controls.Add(this.pbInventory13);
            this.pnlInventory.Controls.Add(this.pbInventory12);
            this.pnlInventory.Controls.Add(this.pbInventory11);
            this.pnlInventory.Controls.Add(this.pbInventory10);
            this.pnlInventory.Controls.Add(this.pbInventory9);
            this.pnlInventory.Controls.Add(this.pbInventory8);
            this.pnlInventory.Controls.Add(this.pbInventory7);
            this.pnlInventory.Controls.Add(this.pbInventory6);
            this.pnlInventory.Controls.Add(this.pbInventory5);
            this.pnlInventory.Controls.Add(this.pbInventory4);
            this.pnlInventory.Controls.Add(this.pbInventory3);
            this.pnlInventory.Controls.Add(this.pbInventory2);
            this.pnlInventory.Controls.Add(this.pbInventory1);
            this.pnlInventory.Location = new System.Drawing.Point(12, 12);
            this.pnlInventory.Name = "pnlInventory";
            this.pnlInventory.Size = new System.Drawing.Size(273, 253);
            this.pnlInventory.TabIndex = 1;
            // 
            // pbInventory36
            // 
            this.pbInventory36.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory36.Location = new System.Drawing.Point(219, 210);
            this.pbInventory36.Name = "pbInventory36";
            this.pbInventory36.Size = new System.Drawing.Size(34, 34);
            this.pbInventory36.TabIndex = 34;
            this.pbInventory36.TabStop = false;
            // 
            // pbInventory35
            // 
            this.pbInventory35.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory35.Location = new System.Drawing.Point(179, 210);
            this.pbInventory35.Name = "pbInventory35";
            this.pbInventory35.Size = new System.Drawing.Size(34, 34);
            this.pbInventory35.TabIndex = 33;
            this.pbInventory35.TabStop = false;
            // 
            // pbInventory34
            // 
            this.pbInventory34.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory34.Location = new System.Drawing.Point(139, 210);
            this.pbInventory34.Name = "pbInventory34";
            this.pbInventory34.Size = new System.Drawing.Size(34, 34);
            this.pbInventory34.TabIndex = 32;
            this.pbInventory34.TabStop = false;
            // 
            // pbInventory33
            // 
            this.pbInventory33.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory33.Location = new System.Drawing.Point(99, 210);
            this.pbInventory33.Name = "pbInventory33";
            this.pbInventory33.Size = new System.Drawing.Size(34, 34);
            this.pbInventory33.TabIndex = 31;
            this.pbInventory33.TabStop = false;
            // 
            // pbInventory32
            // 
            this.pbInventory32.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory32.Location = new System.Drawing.Point(59, 210);
            this.pbInventory32.Name = "pbInventory32";
            this.pbInventory32.Size = new System.Drawing.Size(34, 34);
            this.pbInventory32.TabIndex = 30;
            this.pbInventory32.TabStop = false;
            // 
            // pbInventory31
            // 
            this.pbInventory31.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory31.Location = new System.Drawing.Point(20, 210);
            this.pbInventory31.Name = "pbInventory31";
            this.pbInventory31.Size = new System.Drawing.Size(34, 34);
            this.pbInventory31.TabIndex = 29;
            this.pbInventory31.TabStop = false;
            // 
            // pbInventory30
            // 
            this.pbInventory30.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory30.Location = new System.Drawing.Point(219, 170);
            this.pbInventory30.Name = "pbInventory30";
            this.pbInventory30.Size = new System.Drawing.Size(34, 34);
            this.pbInventory30.TabIndex = 28;
            this.pbInventory30.TabStop = false;
            // 
            // pbInventory29
            // 
            this.pbInventory29.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory29.Location = new System.Drawing.Point(179, 170);
            this.pbInventory29.Name = "pbInventory29";
            this.pbInventory29.Size = new System.Drawing.Size(34, 34);
            this.pbInventory29.TabIndex = 27;
            this.pbInventory29.TabStop = false;
            // 
            // pbInventory28
            // 
            this.pbInventory28.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory28.Location = new System.Drawing.Point(139, 170);
            this.pbInventory28.Name = "pbInventory28";
            this.pbInventory28.Size = new System.Drawing.Size(34, 34);
            this.pbInventory28.TabIndex = 26;
            this.pbInventory28.TabStop = false;
            // 
            // pbInventory27
            // 
            this.pbInventory27.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory27.Location = new System.Drawing.Point(99, 170);
            this.pbInventory27.Name = "pbInventory27";
            this.pbInventory27.Size = new System.Drawing.Size(34, 34);
            this.pbInventory27.TabIndex = 25;
            this.pbInventory27.TabStop = false;
            // 
            // pbInventory26
            // 
            this.pbInventory26.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory26.Location = new System.Drawing.Point(59, 170);
            this.pbInventory26.Name = "pbInventory26";
            this.pbInventory26.Size = new System.Drawing.Size(34, 34);
            this.pbInventory26.TabIndex = 24;
            this.pbInventory26.TabStop = false;
            // 
            // pbInventory25
            // 
            this.pbInventory25.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory25.Location = new System.Drawing.Point(20, 170);
            this.pbInventory25.Name = "pbInventory25";
            this.pbInventory25.Size = new System.Drawing.Size(34, 34);
            this.pbInventory25.TabIndex = 23;
            this.pbInventory25.TabStop = false;
            // 
            // pbInventory24
            // 
            this.pbInventory24.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory24.Location = new System.Drawing.Point(219, 130);
            this.pbInventory24.Name = "pbInventory24";
            this.pbInventory24.Size = new System.Drawing.Size(34, 34);
            this.pbInventory24.TabIndex = 22;
            this.pbInventory24.TabStop = false;
            // 
            // pbInventory23
            // 
            this.pbInventory23.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory23.Location = new System.Drawing.Point(179, 130);
            this.pbInventory23.Name = "pbInventory23";
            this.pbInventory23.Size = new System.Drawing.Size(34, 34);
            this.pbInventory23.TabIndex = 21;
            this.pbInventory23.TabStop = false;
            // 
            // pbInventory22
            // 
            this.pbInventory22.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory22.Location = new System.Drawing.Point(139, 130);
            this.pbInventory22.Name = "pbInventory22";
            this.pbInventory22.Size = new System.Drawing.Size(34, 34);
            this.pbInventory22.TabIndex = 20;
            this.pbInventory22.TabStop = false;
            // 
            // pbInventory21
            // 
            this.pbInventory21.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory21.Location = new System.Drawing.Point(99, 130);
            this.pbInventory21.Name = "pbInventory21";
            this.pbInventory21.Size = new System.Drawing.Size(34, 34);
            this.pbInventory21.TabIndex = 19;
            this.pbInventory21.TabStop = false;
            // 
            // pbInventory20
            // 
            this.pbInventory20.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory20.Location = new System.Drawing.Point(59, 130);
            this.pbInventory20.Name = "pbInventory20";
            this.pbInventory20.Size = new System.Drawing.Size(34, 34);
            this.pbInventory20.TabIndex = 18;
            this.pbInventory20.TabStop = false;
            // 
            // pbInventory19
            // 
            this.pbInventory19.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory19.Location = new System.Drawing.Point(20, 130);
            this.pbInventory19.Name = "pbInventory19";
            this.pbInventory19.Size = new System.Drawing.Size(34, 34);
            this.pbInventory19.TabIndex = 17;
            this.pbInventory19.TabStop = false;
            // 
            // pbInventory18
            // 
            this.pbInventory18.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory18.Location = new System.Drawing.Point(219, 90);
            this.pbInventory18.Name = "pbInventory18";
            this.pbInventory18.Size = new System.Drawing.Size(34, 34);
            this.pbInventory18.TabIndex = 16;
            this.pbInventory18.TabStop = false;
            // 
            // pbInventory17
            // 
            this.pbInventory17.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory17.Location = new System.Drawing.Point(179, 90);
            this.pbInventory17.Name = "pbInventory17";
            this.pbInventory17.Size = new System.Drawing.Size(34, 34);
            this.pbInventory17.TabIndex = 15;
            this.pbInventory17.TabStop = false;
            // 
            // pbInventory16
            // 
            this.pbInventory16.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory16.Location = new System.Drawing.Point(139, 90);
            this.pbInventory16.Name = "pbInventory16";
            this.pbInventory16.Size = new System.Drawing.Size(34, 34);
            this.pbInventory16.TabIndex = 14;
            this.pbInventory16.TabStop = false;
            // 
            // pbInventory15
            // 
            this.pbInventory15.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory15.Location = new System.Drawing.Point(99, 90);
            this.pbInventory15.Name = "pbInventory15";
            this.pbInventory15.Size = new System.Drawing.Size(34, 34);
            this.pbInventory15.TabIndex = 13;
            this.pbInventory15.TabStop = false;
            // 
            // pbInventory14
            // 
            this.pbInventory14.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory14.Location = new System.Drawing.Point(59, 90);
            this.pbInventory14.Name = "pbInventory14";
            this.pbInventory14.Size = new System.Drawing.Size(34, 34);
            this.pbInventory14.TabIndex = 12;
            this.pbInventory14.TabStop = false;
            // 
            // pbInventory13
            // 
            this.pbInventory13.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory13.Location = new System.Drawing.Point(20, 90);
            this.pbInventory13.Name = "pbInventory13";
            this.pbInventory13.Size = new System.Drawing.Size(34, 34);
            this.pbInventory13.TabIndex = 5;
            this.pbInventory13.TabStop = false;
            // 
            // pbInventory12
            // 
            this.pbInventory12.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory12.Location = new System.Drawing.Point(219, 50);
            this.pbInventory12.Name = "pbInventory12";
            this.pbInventory12.Size = new System.Drawing.Size(34, 34);
            this.pbInventory12.TabIndex = 11;
            this.pbInventory12.TabStop = false;
            // 
            // pbInventory11
            // 
            this.pbInventory11.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory11.Location = new System.Drawing.Point(179, 50);
            this.pbInventory11.Name = "pbInventory11";
            this.pbInventory11.Size = new System.Drawing.Size(34, 34);
            this.pbInventory11.TabIndex = 10;
            this.pbInventory11.TabStop = false;
            // 
            // pbInventory10
            // 
            this.pbInventory10.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory10.Location = new System.Drawing.Point(139, 50);
            this.pbInventory10.Name = "pbInventory10";
            this.pbInventory10.Size = new System.Drawing.Size(34, 34);
            this.pbInventory10.TabIndex = 9;
            this.pbInventory10.TabStop = false;
            // 
            // pbInventory9
            // 
            this.pbInventory9.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory9.Location = new System.Drawing.Point(99, 50);
            this.pbInventory9.Name = "pbInventory9";
            this.pbInventory9.Size = new System.Drawing.Size(34, 34);
            this.pbInventory9.TabIndex = 8;
            this.pbInventory9.TabStop = false;
            // 
            // pbInventory8
            // 
            this.pbInventory8.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory8.Location = new System.Drawing.Point(59, 50);
            this.pbInventory8.Name = "pbInventory8";
            this.pbInventory8.Size = new System.Drawing.Size(34, 34);
            this.pbInventory8.TabIndex = 7;
            this.pbInventory8.TabStop = false;
            // 
            // pbInventory7
            // 
            this.pbInventory7.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory7.Location = new System.Drawing.Point(20, 50);
            this.pbInventory7.Name = "pbInventory7";
            this.pbInventory7.Size = new System.Drawing.Size(34, 34);
            this.pbInventory7.TabIndex = 6;
            this.pbInventory7.TabStop = false;
            // 
            // pbInventory6
            // 
            this.pbInventory6.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory6.Location = new System.Drawing.Point(219, 11);
            this.pbInventory6.Name = "pbInventory6";
            this.pbInventory6.Size = new System.Drawing.Size(34, 34);
            this.pbInventory6.TabIndex = 5;
            this.pbInventory6.TabStop = false;
            // 
            // pbInventory5
            // 
            this.pbInventory5.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory5.Location = new System.Drawing.Point(179, 11);
            this.pbInventory5.Name = "pbInventory5";
            this.pbInventory5.Size = new System.Drawing.Size(34, 34);
            this.pbInventory5.TabIndex = 4;
            this.pbInventory5.TabStop = false;
            // 
            // pbInventory4
            // 
            this.pbInventory4.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory4.Location = new System.Drawing.Point(139, 11);
            this.pbInventory4.Name = "pbInventory4";
            this.pbInventory4.Size = new System.Drawing.Size(34, 34);
            this.pbInventory4.TabIndex = 3;
            this.pbInventory4.TabStop = false;
            // 
            // pbInventory3
            // 
            this.pbInventory3.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory3.Location = new System.Drawing.Point(99, 11);
            this.pbInventory3.Name = "pbInventory3";
            this.pbInventory3.Size = new System.Drawing.Size(34, 34);
            this.pbInventory3.TabIndex = 2;
            this.pbInventory3.TabStop = false;
            // 
            // pbInventory2
            // 
            this.pbInventory2.BackColor = System.Drawing.Color.Transparent;
            this.pbInventory2.Location = new System.Drawing.Point(59, 11);
            this.pbInventory2.Name = "pbInventory2";
            this.pbInventory2.Size = new System.Drawing.Size(34, 34);
            this.pbInventory2.TabIndex = 1;
            this.pbInventory2.TabStop = false;
            // 
            // btnBag1
            // 
            this.btnBag1.BackgroundImage = global::RPGInventory.Properties.Resources.btnBag1_inactive;
            this.btnBag1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBag1.Location = new System.Drawing.Point(3, 3);
            this.btnBag1.Name = "btnBag1";
            this.btnBag1.Size = new System.Drawing.Size(58, 23);
            this.btnBag1.TabIndex = 2;
            this.btnBag1.UseVisualStyleBackColor = true;
            this.btnBag1.Click += new System.EventHandler(this.btnBag_Click);
            this.btnBag1.MouseLeave += new System.EventHandler(this.btnBag_MouseLeave);
            this.btnBag1.MouseHover += new System.EventHandler(this.btnBag_MouseHover);
            // 
            // btnBag2
            // 
            this.btnBag2.BackgroundImage = global::RPGInventory.Properties.Resources.btnBag2_inactive;
            this.btnBag2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBag2.Location = new System.Drawing.Point(64, 3);
            this.btnBag2.Name = "btnBag2";
            this.btnBag2.Size = new System.Drawing.Size(58, 23);
            this.btnBag2.TabIndex = 3;
            this.btnBag2.UseVisualStyleBackColor = true;
            this.btnBag2.Click += new System.EventHandler(this.btnBag_Click);
            this.btnBag2.MouseLeave += new System.EventHandler(this.btnBag_MouseLeave);
            this.btnBag2.MouseHover += new System.EventHandler(this.btnBag_MouseHover);
            // 
            // btnBag3
            // 
            this.btnBag3.BackgroundImage = global::RPGInventory.Properties.Resources.btnBag3_inactive;
            this.btnBag3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBag3.Location = new System.Drawing.Point(124, 3);
            this.btnBag3.Name = "btnBag3";
            this.btnBag3.Size = new System.Drawing.Size(58, 23);
            this.btnBag3.TabIndex = 4;
            this.btnBag3.UseVisualStyleBackColor = true;
            this.btnBag3.Click += new System.EventHandler(this.btnBag_Click);
            this.btnBag3.MouseLeave += new System.EventHandler(this.btnBag_MouseLeave);
            this.btnBag3.MouseHover += new System.EventHandler(this.btnBag_MouseHover);
            // 
            // btnBag4
            // 
            this.btnBag4.BackgroundImage = global::RPGInventory.Properties.Resources.btnBag4_inactive;
            this.btnBag4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBag4.Location = new System.Drawing.Point(185, 3);
            this.btnBag4.Name = "btnBag4";
            this.btnBag4.Size = new System.Drawing.Size(58, 23);
            this.btnBag4.TabIndex = 5;
            this.btnBag4.UseVisualStyleBackColor = true;
            this.btnBag4.Click += new System.EventHandler(this.btnBag_Click);
            this.btnBag4.MouseLeave += new System.EventHandler(this.btnBag_MouseLeave);
            this.btnBag4.MouseHover += new System.EventHandler(this.btnBag_MouseHover);
            // 
            // pnlButtons
            // 
            this.pnlButtons.BackColor = System.Drawing.Color.Transparent;
            this.pnlButtons.Controls.Add(this.btnBag4);
            this.pnlButtons.Controls.Add(this.btnBag1);
            this.pnlButtons.Controls.Add(this.btnBag3);
            this.pnlButtons.Controls.Add(this.btnBag2);
            this.pnlButtons.Location = new System.Drawing.Point(26, 265);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(248, 31);
            this.pnlButtons.TabIndex = 6;
            // 
            // FrmInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::RPGInventory.Properties.Resources.inventory;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(600, 300);
            this.Controls.Add(this.pnlButtons);
            this.Controls.Add(this.pnlInventory);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmInventory";
            this.Text = "Inventory";
            this.Load += new System.EventHandler(this.FrmInventory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory1)).EndInit();
            this.pnlInventory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbInventory2)).EndInit();
            this.pnlButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbInventory1;
        private System.Windows.Forms.Panel pnlInventory;
        private System.Windows.Forms.PictureBox pbInventory2;
        private System.Windows.Forms.PictureBox pbInventory6;
        private System.Windows.Forms.PictureBox pbInventory5;
        private System.Windows.Forms.PictureBox pbInventory4;
        private System.Windows.Forms.PictureBox pbInventory3;
        private System.Windows.Forms.PictureBox pbInventory36;
        private System.Windows.Forms.PictureBox pbInventory35;
        private System.Windows.Forms.PictureBox pbInventory34;
        private System.Windows.Forms.PictureBox pbInventory33;
        private System.Windows.Forms.PictureBox pbInventory32;
        private System.Windows.Forms.PictureBox pbInventory31;
        private System.Windows.Forms.PictureBox pbInventory30;
        private System.Windows.Forms.PictureBox pbInventory29;
        private System.Windows.Forms.PictureBox pbInventory28;
        private System.Windows.Forms.PictureBox pbInventory27;
        private System.Windows.Forms.PictureBox pbInventory26;
        private System.Windows.Forms.PictureBox pbInventory25;
        private System.Windows.Forms.PictureBox pbInventory24;
        private System.Windows.Forms.PictureBox pbInventory23;
        private System.Windows.Forms.PictureBox pbInventory22;
        private System.Windows.Forms.PictureBox pbInventory21;
        private System.Windows.Forms.PictureBox pbInventory20;
        private System.Windows.Forms.PictureBox pbInventory19;
        private System.Windows.Forms.PictureBox pbInventory18;
        private System.Windows.Forms.PictureBox pbInventory17;
        private System.Windows.Forms.PictureBox pbInventory16;
        private System.Windows.Forms.PictureBox pbInventory15;
        private System.Windows.Forms.PictureBox pbInventory14;
        private System.Windows.Forms.PictureBox pbInventory13;
        private System.Windows.Forms.PictureBox pbInventory12;
        private System.Windows.Forms.PictureBox pbInventory11;
        private System.Windows.Forms.PictureBox pbInventory10;
        private System.Windows.Forms.PictureBox pbInventory9;
        private System.Windows.Forms.PictureBox pbInventory8;
        private System.Windows.Forms.PictureBox pbInventory7;
        private System.Windows.Forms.Button btnBag1;
        private System.Windows.Forms.Button btnBag2;
        private System.Windows.Forms.Button btnBag3;
        private System.Windows.Forms.Button btnBag4;
        private System.Windows.Forms.Panel pnlButtons;
    }
}